## The Assignment ##

Use the Giphy API to create an application which allows users to search, view, and favourite gifs.

## Specifications ##

A tab bar with 2 tabs - feed and favourites

## First Tab ##
* Contains a search bar at the top.
* Contains a table view that displays searched gifs.
* Loading indicator while searching
* The default items in the table view should be the trending gifs (if nothing in search bar)

## Second Tab ##
* Contains a collection view that displays a grid of favourited gifs stored locally.

## Gif Table View Cell ##
* Should contain a view of the gif running.
* Should contain a favourite button which allows favouriting and unfavouriting.
* The favourited items should be stored locally on the device.

## Gif Collection View Cell ##
* Should contain a view of the gif running.
* Should contain an unfavourite button

## Bonus ##
* Use iOS MVVM architecture instead of MVC
* Use RxSwift
* Add pagination (infinite scrolling) to the gif lists.